package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "event")
@NoArgsConstructor
public class Event {

    //Automatically generate ID
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "eventId", updatable = false, nullable = false)
    private int eventId;

    @Column(name = "eventName")
    private String eventName;

    @Column(name = "eventDate")
    private String eventDate;

    @Column(name = "eventTime")
    private String eventTime;

    @Column(name = "eventLocation")
    private String eventLocation;

    @Column(name = "eventCategory")
    private String eventCategory;

    @Column(name = "eventDescription")
    private String eventDescription;

    //Get creator's ID --> email
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "email")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
    private List<Guest> guestList;

    public Event(String eventName, String eventDate, String eventTime, String eventLocation, String eventCategory, String eventDescription){
        this.eventName = eventName;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventLocation = eventLocation;
        this.eventDescription = eventDescription;
        this.guestList = new ArrayList<>();
    }

}
