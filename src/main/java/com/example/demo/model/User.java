package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "user")
@NoArgsConstructor
public class User {

    //Using email as ID
    @Id
    @Column(name = "userEmail", updatable = false, nullable = false)
    private String email;

    @Column(name = "userName")
    private String userName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private List<Event> events;

    public User(String email, String userName){
        this.email = email;
        this.userName = userName;
        this.events = new ArrayList<>();
    }
}
