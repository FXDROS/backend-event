package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@Table(name = "guest")
@NoArgsConstructor
public class Guest {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "guestId", updatable = false, nullable = false)
    private int guestId;

    @Column(name = "guestName")
    private String guestName;

    @Column(name = "isAccepted")
    private boolean isAccepted;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "eventId")
    private Event event;

    public Guest(String guestName){
        this.guestName = guestName;
        this.isAccepted = false;
    }
}
